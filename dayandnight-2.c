#include <stdio.h>
#include <stdlib.h>
#include<mpi.h> 
#include <time.h>



void State_Init (int High,int Width,int *Canvas )
{
	
	srand(time(NULL));//初始化随机数 
	for(int i=0;i<High;i++)
	{
		for(int j=0;j<Width;j++)
		{
			Canvas[i*Width+j]=rand()%2;//随机赋值  
		}
	}
}

void Show (int High,int Width,int *Canvas2 )//生输出1 死输出 0
{
	
	for(int i=0;i<High;i++)
	{
		for(int j=0;j<Width;j++)
		{
			if(Canvas2[i*Width+j]==1)
				printf("1");
				else
					printf("0");
		}
		printf("\n");
	
	}
}


void Auto_Up(int rank,int sz,int High,int Width,int *tmp )//自动更新 
{
	 
	
for(int t=0;t<10;t++)
	{	
	for(int i=0;i<High;i++)
	{
		int tt,t2;
		tt=i-1;
			if(i-1<0){
				tt=0;
			}
			t2=i+1;
			if(i+1>High){
				t2=High-1;
			}
		for(int j=0;j<Width;j++)//比较缓存后的值在更新原本值 
		{
			int t3,t4;
			
			if(j-1<0){
				t3=0;
			}
			t4=j+1;
			if(j+1>Width){
				t4=Width-1;
			}
			int RoundNum=tmp[(tt)*Width+t3]+tmp[(tt)*Width+j]+tmp[(tt)*Width+t4]
					+tmp[i*Width+t3] +tmp[i*Width+t4]
					+tmp[(t2)*Width+t3]+tmp[(t2)*Width+j]+tmp[(t2)*Width+t4];//周围的个数 //将附近8个全部相加 
			if(RoundNum==3 || RoundNum==4 || RoundNum==6 || RoundNum==7 || RoundNum==8)
					tmp[i*Width+j]=1;
			else 
				tmp[i*Width+j]=0;	
		}
	}
	}
	
}		

int main(int argc, char** argv)
{
	int High =20;
	int lh,Width =50,ss=0;
	
    High=atoi(argv[1]);
	Width=atoi(argv[2]);
	int hw=High*Width;

	int *Canvas;
	int *dc;
	int rank = 0, sz = 0;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &sz);
    MPI_Status status;
	Canvas=(int*)malloc(High*Width*sizeof(int));
	dc=(int*)malloc(High*Width*sizeof(int));
	if(rank==0){
		State_Init(High,Width,Canvas);
		
		//Show(High,Width,Canvas);
	}	
	
	lh=High/sz;
	
	MPI_Scatter(Canvas,lh*Width,MPI_INT,dc,lh*Width,MPI_INT,0, MPI_COMM_WORLD);
	
	Auto_Up(rank,sz,lh,Width,dc);
	MPI_Gather(dc,lh*Width,MPI_INT,Canvas,lh*Width,MPI_INT,0, MPI_COMM_WORLD);
	MPI_Barrier(MPI_COMM_WORLD);
	if(rank==0){
	Show(High,Width,Canvas);
	}
	
	free(Canvas);
	free(dc);
	MPI_Finalize();
	//
	return 0;
}
